#!/bin/sh

# debugging...
set -xv
set -o errexit

ffmpeg='/usr/local/bin/ffmpeg'

pubdir='/Library/Server/Web/Data/Sites/Default/ski_movies/'
imgpath='http://cameras.skibanff.com/132l'
imgname='132l'
fontpath='/Library/Fonts/Georgia.ttf'

imgdir='images_132l'
tempdir='movie_divide'
moviename='divide'
mkdir -p $tempdir
mkdir -p $imgdir

# fancy rm syntax to avoid error if directory is empty
rm $tempdir/* 2>/dev/null || true

# This gets a new file
# curl $imgpath > "${imgdir}/${imgname}-$(date +"%Y%m%d-%H%M%S").jpg"

idx=0

#find $imgdir -type f -ctime 2 | sort


for file in $(find $imgdir -type f -mtime -2 | sort )
do
  idx0=$(printf "%04d" $idx)
  ln $file $tempdir/image-$idx0.jpg
  idx=$(( $idx + 1 ))
done

rm -f ${moviename}.mp4

# -r 3 is 3 frames per image
${ffmpeg} -n -i "$tempdir/image-%04d.jpg" \
  -vcodec libx264  \
  -pix_fmt yuv420p \
  -r 3 \
  ${moviename}.mp4

rm -f ${moviename}_dated.mp4

title='Sunshine Village '$moviename' '$(date)

# Put a title on it
${ffmpeg} -i ${moviename}.mp4 \
	-vf drawtext="text=\'${title}\': fontfile=${fontpath}: fontcolor=white: \
fontsize=24: box=1: boxcolor=black@0.5: \
boxborderw=5: x=60: y=60" \
	-codec:a copy ${moviename}_dated.mp4

# move it into position

mv -f ${moviename}_dated.mp4 ${pubdir}