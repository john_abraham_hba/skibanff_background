#!/bin/sh

# debugging...
set -xv
set -o errexit

ffmpeg='/usr/local/bin/ffmpeg'

pubdir='/Library/Server/Web/Data/Sites/Default/ski_movies/'
fontpath='/Library/Fonts/Arial Unicode.ttf'

images=( '132l' '131l' '15l' )

for imgname in "${images[@]}"
do 

imgpath="http://cameras.skibanff.com/${imgname}"
imgdir="images_${imgname}"
tempdir="movie_${imgname}"
moviename="$movie_${imgname}"
mkdir -p $tempdir
mkdir -p $imgdir

# fancy rm syntax to avoid error if directory is empty
rm $tempdir/* 2>/dev/null || true

idx=0

for file in $(find $imgdir -type f -mtime -2 | sort )
do
  idx0=$(printf "%04d" $idx)
  ln $file $tempdir/image-$idx0.jpg
  idx=$(( $idx + 1 ))
done

rm -f ${moviename}.mp4

title='Sunshine Village '$moviename' '$(date)

# -r 3 is 3 frames per image if you want
${ffmpeg} -n -i "$tempdir/image-%04d.jpg" \
  -vcodec libx264  \
  -pix_fmt yuv420p \
	-vf drawtext="text=\'${title}\': fontfile=${fontpath}: fontcolor=white: \
fontsize=24: box=1: boxcolor=black@0.5: \
boxborderw=5: x=60: y=60" \
  ${moviename}.mp4

rm -f ${moviename}_dated.mp4

# Put a title on it, but now we do the title at the same time as coding it up, rather than asking ffmpeg to do more work
#${ffmpeg} -i ${moviename}.mp4 \
#	-vf drawtext="text=\'${title}\': fontfile=${fontpath}: fontcolor=white: \
#fontsize=24: box=1: boxcolor=black@0.5: \
#boxborderw=5: x=60: y=60" \
#	-codec:a copy ${moviename}_dated.mp4


cp ${moviename}.mp4 ${moviename}_dated.mp4

# move it into position

mv -f ${moviename}_dated.mp4 ${pubdir}

done
