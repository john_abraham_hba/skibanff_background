#!/bin/sh

# debugging...
set -xv
set -o errexit

images=( '132l' '134l' '131l' '135l', '15l')

echo "images is ${images}"

for imgname in "${images[@]}"
do 
    imgpath="http://cameras.skibanff.com/${imgname}"
    imgdir="images_${imgname}"
    mkdir -p $imgdir

    filename="${imgname}-$(date +"%Y%m%d-%H%M%S").jpg"
    # This gets a new file
    curl $imgpath > "${imgdir}/${filename}"

    if sips -g pixelWidth "${imgdir}/${filename}" | grep nil; then
	rm "${imgdir}/${filename}"
    fi

    if ! sips -g pixelWidth "${imgdir}/${filename}"; then 
	rm "${imgdir}/${filename}"
    fi


done
